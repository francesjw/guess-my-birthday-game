from random import randint

player_name = input("Hi! What is your name?")
# tries to guess birth month and year

for guess_number in range(1,6):
    birth_year = randint(1900,2023)
    birth_month = randint(1,12)
    print("Guess", guess_number, ":",
          player_name,
          "were you born in", birth_month,
          "/", birth_year, "?")

    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
